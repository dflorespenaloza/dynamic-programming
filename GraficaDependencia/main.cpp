#include <iostream>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include "Node.h"
#include "NodeDepend.h"
#include "AVLTree.h"
#include "DependencyGraph.h"

class WeightBenefits{
	public:
		unsigned int b[4];
		unsigned int w[4];

		WeightBenefits(){
			b[0] = 10; 
			w[0] = 5;
			b[1] = 40;
			w[1] = 4;
			b[2] = 30;
			w[2] = 6;
			b[3] = 50;
			w[3] = 3;
		}
};

class KnapsackGraph{
	private:
		unsigned int n;
		unsigned int W;
		WeightBenefits* ex_p;

	public:
		KnapsackGraph(){
			n = 4;
			W = 10; 
			ex_p = new WeightBenefits();
		}

		unsigned int getInicial(){
			return (n-1)*(W+1) + W;
		}

		std::vector<unsigned int>* getSucessors(unsigned int key){
			unsigned int i = key/(W+1);
			unsigned int j = key%(W+1);
			if(j<ex_p->w[i] || i==0){
				return NULL;
			}else{
				std::vector<unsigned int>* sucesores = new std::vector<unsigned int>(2);
				sucesores->at(0) = (i-1)*(W+1) + j;
				sucesores->at(1) = (i-1)*(W+1) + j-ex_p->w[i];
				return sucesores;
			}
		}
};

int main(){
	KnapsackGraph* knGraph = new KnapsackGraph();
	DependencyGraph<KnapsackGraph>* graph = new DependencyGraph<KnapsackGraph>(knGraph);
	
	delete knGraph;
	delete graph;	
	return EXIT_SUCCESS;
}

class NodeDepend{
	private:
		unsigned int key;
		unsigned int numDependencies;
		std::vector<NodeDepend*>* dependencies;

	public:
		NodeDepend(unsigned int key){
			numDependencies = 0;
			this->key = key;
			dependencies = new std::vector<NodeDepend*>();
		}

		unsigned int getKey(){
			return key;
		}

		void addToDependencies(NodeDepend* node){
			dependencies->push_back(node);
		}

		std::vector<NodeDepend*>* getDependencies(){
			return dependencies;
		}

		unsigned int getNumDepend(){
			return numDependencies;
		}

		void decreaseNumDepend(){
			numDependencies--;
		}

		void setNumDepend(unsigned int n){
			numDependencies = n;
		}
};

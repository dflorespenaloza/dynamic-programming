template <class T>
class Node{
	private:
		int key;
		T* data;
		unsigned int height;
		Node<T>* parent;
		Node<T>* left_child;
		Node<T>* right_child;

	public:
		Node(int key, T* data){
			this->key = key;
			this->data = data; 
			height = 0;
			parent = NULL;
			left_child = NULL;
			right_child = NULL;
		}

		int getKey(){
			return key;
		}

		unsigned int getHeight(){
			return height;
		}

		Node<T>* getParent(){
			return parent;
		}

		Node<T>* getLeftChild(){
			return left_child;
		}

		Node<T>* getRightChild(){
			return right_child;
		}

		T* getData(){
			return data;
		}
		
		void removeParent(){
			parent = NULL;
		}

		void setLeftChild(Node<T>* new_left){
			this->left_child = new_left;
			if(new_left != NULL) 
				new_left->parent = this;
			updateHeight();
		}
                               
		void setRightChild(Node<T>* new_right){
			right_child = new_right;
			if(new_right != NULL)
				new_right->parent = this;
			updateHeight();
		}

		void updateHeight(){
			if(left_child != NULL && right_child != NULL){
				if(left_child->getHeight() > right_child->getHeight())
					height = left_child->getHeight() + 1;
				else
					height = right_child->getHeight() + 1;
			}else if(left_child != NULL)
				height = left_child->getHeight() + 1;
			else if(right_child != NULL)
				height = right_child->getHeight() + 1;
			else
				height = 0;
		}

		int getBalance(){
			if(getLeftChild()!= NULL && getRightChild()!=NULL)
				return getLeftChild()->getHeight() - getRightChild()->getHeight();
			else if(getLeftChild() != NULL)
				return getLeftChild()->getHeight() + 1;
			else if(getRightChild() != NULL)
				return -(getRightChild()->getHeight() + 1);
			else
				return 0;
		}
};

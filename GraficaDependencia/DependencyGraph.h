template <typename U>
class DependencyGraph{
	private:
		AVLTree<NodeDepend>* tree;
		NodeDepend* root;
		std::vector<NodeDepend*>* casosBase;
		U* classSucessors;
		
		void addSucessors(NodeDepend* node){
			std::vector<unsigned int>* sucess = classSucessors->getSucessors(node->getKey());
			if(sucess == NULL){
				casosBase->push_back(node);
			}else{
				node->setNumDepend(sucess->size());
				for(unsigned i=0; i < sucess->size(); i++){
					Node<NodeDepend>* find = tree->findNode(sucess->at(i));
					if(find != NULL){
						find->getData()->addToDependencies(node);
					}else{
						NodeDepend* new_v = new NodeDepend(sucess->at(i));
						tree->insert(sucess->at(i), new_v);
						new_v->addToDependencies(node);
						addSucessors(new_v);
					}
				}			
			}
		}
	public:
		DependencyGraph(U* classSucessors){
			this->classSucessors = classSucessors;
			casosBase = new std::vector<NodeDepend*>();
			unsigned int keyInitial = classSucessors->getInicial(); 
			root = new NodeDepend(keyInitial);
			tree = new AVLTree<NodeDepend>(keyInitial, root);

			addSucessors(root);
			delete tree;

			generaCasos();
		}

		void generaCasos(){
			int n = 0;
			while(casosBase->size()!=0){
				std::cout<<"CASO n = " << n++ <<"\n";
				printCasosBase();
				newCasesBases();
			}
		}

		void printCasosBase(){
			for(unsigned i=0; i < casosBase->size(); i++){
				std::cout<<casosBase->at(i)->getKey()<<" \n";
			}
		}

		void newCasesBases(){
			std::vector<NodeDepend*>* new_casosBase = new std::vector<NodeDepend*>();

			for(unsigned i=0; i < casosBase->size(); i++){
				NodeDepend* to_delete = casosBase->at(i);
				std::vector<NodeDepend*>* dep_to_delete = to_delete->getDependencies();
				for(unsigned j=0; j < dep_to_delete->size(); j++){
					dep_to_delete->at(j)->decreaseNumDepend();
					if(dep_to_delete->at(j)->getNumDepend()==0){
						new_casosBase->push_back(dep_to_delete->at(j));
					}
				}
				delete to_delete;
			}
			delete casosBase;
			casosBase = new_casosBase;
		}

		~DependencyGraph(){
			delete root;
			delete classSucessors;
		}
};

template <class T>
class AVLTree{
	private:
		Node<T>* root;

		void setRoot(Node<T>* node){
			root = node;
			if(root != NULL)
				root->removeParent();
                }

		void rotateLeft(Node<T>* node){
			Node<T>* p = node->getParent();
                        enum {left, right} side;
			if(p != NULL){
				if(p->getLeftChild() == node)
					side = left;
				else
					side = right;
                        }

                        Node<T>* temp = node->getRightChild();
                        node->setRightChild(temp->getLeftChild());
                        temp->setLeftChild(node);

                        // Now attach the subtree to the parent (or root)
                        if(p != NULL){
				if(side == left)
					p->setLeftChild(temp);
				if(side == right)
					p->setRightChild(temp);
                        }else{
				setRoot(temp);
                        }
                }

		void rotateRight(Node<T>* node){
			Node<T>* p = node->getParent();
                        enum {left, right} side;
			if(p != NULL){
				if(p->getLeftChild() == node)
					side = left;
				else
					side = right;
			}
                        Node<T>* temp = node->getLeftChild();
                        node->setLeftChild(temp->getRightChild());
                        temp->setRightChild(node);

                        // Now attach the subtree to the parent (or root)
                        if(p != NULL){
				if(side == left)
					p->setLeftChild(temp);
				if(side == right)
					p->setRightChild(temp);
                        }else{
				setRoot(temp);
			}
		}

		// This function does balancing at the given node
		void balanceAtNode(Node<T>* node){
			int bal = node->getBalance();
			if(bal > 1){
				if(node->getLeftChild()->getBalance() < 0)
					rotateLeft(node->getLeftChild());
                                rotateRight(node);
			}else if(bal < -1){
				if(node->getRightChild()->getBalance() > 0)
					rotateRight(node->getRightChild());
				rotateLeft(node);
                        }
                }

		void deleteTree(Node<T>* node){
			if(node != NULL){
				deleteTree(node->getLeftChild());
				deleteTree(node->getRightChild());
				delete node;
   			}
		}

		void printFromNode(Node<T>* node){
			if(node != NULL){
				std::cout<<"Node: "<< node->getKey() << " heigth " << node->getHeight() << "\n";
				if(node->getLeftChild() != NULL){
					std::cout<<"\tmoving left\n\t";
					printFromNode(node->getLeftChild());
					std::cout<<"Returning to Node" << node->getKey() << " from its' left subtree\n";
				}else{
					std::cout<<"\t left subtree is empty\n";
				}
				if(node->getRightChild() != NULL){
					std::cout<<"\tmoving right\n\t";
					printFromNode(node->getRightChild());
					std::cout<<"Returning to Node" <<node->getKey() << " from its' right subtree\n";
				}else
					std::cout<<"\t right subtree is empty\n";
			}
		}


	public:
		AVLTree(){
			root = NULL;
		}
 
		AVLTree(int key, T* data){
			root = new Node<T>(key, data);
		}

		int getHeight(){
			return root->getHeight();
		}

		bool insert(int key, T* data){
			if(root == NULL){
				root = new Node<T>(key, data);
				return true;
                        }else{
				Node<T>* added_node;
				Node<T>* temp = root;
 
                                while(true){
					if(temp->getKey() > key){
						if(temp->getLeftChild() == NULL){
							temp->setLeftChild(new Node<T>(key, data));
							added_node = temp->getLeftChild(); 
							break;
                                                }
                                                else
							temp = temp->getLeftChild();
                                        }
                                        else if(temp->getKey() < key){
						if(temp->getRightChild() == NULL){
							temp->setRightChild(new Node<T>(key, data));
							added_node = temp->getRightChild();
							break;
                                                }else
                                                        temp = temp->getRightChild(); 
					}else{
						return false;
					}
				}

                                // The following code is for updating heights and balancing.
                                temp = added_node;
                                while(temp != NULL){
					temp->updateHeight();
					balanceAtNode(temp);
					temp = temp->getParent();
                                }
				return true;
                        }
                }

		Node<T>* findNode(int key){
			Node<T>* temp = root;
			while(temp != 0){
				if(key == temp->getKey())
					return temp;
				else if(key < temp->getKey())
					temp = temp->getLeftChild();
				else
					temp = temp->getRightChild();
			}
			return NULL;
		}

		bool remove(int key){
			if(root == NULL)
				return false;
			Node<T> *replacement, *replacement_parent, *temp_node;

			Node<T>* to_be_removed = findNode(key);
			if(to_be_removed == NULL)
				return false;
 
			Node<T>* p = to_be_removed->getParent();
 
			enum {left, right} side;
			if(p != NULL){
				if(p->getLeftChild() == to_be_removed)
					side = left;
                                else
					side = right;
			}
 
			int bal = to_be_removed->getBalance();

			if(to_be_removed->getLeftChild() == NULL && to_be_removed->getRightChild() == NULL){
				if(p != 0){
					if(side == left)
						p->setLeftChild(NULL);
					else
						p->setRightChild(NULL);
					p->updateHeight();
					balanceAtNode(p);
				}else
					setRoot(NULL);
			}else if(to_be_removed->getRightChild() == NULL){
				if(p != 0){
					if(side == left)
						p->setLeftChild(to_be_removed->getLeftChild());
                                        else
						p->setRightChild(to_be_removed->getLeftChild());
                                        p->updateHeight();
					balanceAtNode(p);
                                }else
                                        setRoot(to_be_removed->getLeftChild());
                        }else if(to_be_removed->getLeftChild() == NULL){
                                if(p != 0){
					if(side == left)
						p->setLeftChild(to_be_removed->getRightChild());
					else
						p->setRightChild(to_be_removed->getRightChild());
					p->updateHeight();
					balanceAtNode(p);
				}
				else
					setRoot(to_be_removed->getRightChild());
                        }else{
				if(bal > 0){
					if(to_be_removed->getLeftChild()->getRightChild() == NULL){
						replacement = to_be_removed->getLeftChild();
						replacement->setRightChild(to_be_removed->getRightChild());
						temp_node = replacement;
					}else{
						replacement = to_be_removed->getLeftChild()->getRightChild();
                                                while(replacement->getRightChild() != NULL){
							replacement = replacement->getRightChild();
                                                }
						replacement_parent = replacement->getParent();
						replacement_parent->setRightChild(replacement->getLeftChild());
						temp_node = replacement_parent;
						replacement->setLeftChild(to_be_removed->getLeftChild());
						replacement->setRightChild(to_be_removed->getRightChild());
                                        }
                                }else{
					if(to_be_removed->getRightChild()->getLeftChild() == NULL){
						replacement = to_be_removed->getRightChild();
						replacement->setLeftChild(to_be_removed->getLeftChild());
						temp_node = replacement;
                                        }else{
						replacement = to_be_removed->getRightChild()->getLeftChild();
						while(replacement->getLeftChild() != 0){
                                                        replacement = replacement->getLeftChild();
                                                }
						replacement_parent = replacement->getParent();
						replacement_parent->setLeftChild(replacement->getRightChild());
						temp_node = replacement_parent;
						replacement->setLeftChild(to_be_removed->getLeftChild());
						replacement->setRightChild(to_be_removed->getRightChild());
                                        }
                                }               
                                if(p != 0){
					if(side == left)
						p->setLeftChild(replacement);
                                        else
						p->setRightChild(replacement);
                                }else
                                        setRoot(replacement);
                                balanceAtNode(temp_node);
			}
			delete to_be_removed;
			return true;
		}

		void printTree(){
			std::cout << "\nPrinting the tree...\n";
			printFromNode(root);
		}

		~AVLTree(){
			deleteTree(root);
		}
};

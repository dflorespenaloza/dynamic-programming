#include "../dp_Serial.h"

class WeightBenefits{
	public:
		static const unsigned int n = 4;//4095;
		static const unsigned int W = 10;//299999;

		unsigned int w[n];
		unsigned int p[n];

		WeightBenefits(){
			w[0] = 5;
			w[1] = 4;
			w[2] = 6;
			w[3] = 3;

			p[0] = 10;
			p[1] = 40;
			p[2] = 30;
			p[3] = 50;

			/*for(unsigned int i=0; i < 4095; i++){
				w[i] = rand()%1000;
				b[i] = rand()%1000;
			}*/
		}
};

void Knapsack(unsigned int i, unsigned int j, Matrix<unsigned int>* M, WeightBenefits* ex_p){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		unsigned int wi1 = ex_p->w[i-1];
		unsigned int mi1 = M->getAt(i-1, j);
		if(wi1 > j){
			M->setAt(i, j, mi1);
		}else{
			M->setAt(i, j, max(mi1, M->getAt(i-1, j-wi1) + ex_p->p[i-1]));
		}	
	}
}

void fillKnapsack(Matrix<unsigned int>* M, WeightBenefits* ex_p){
	for(unsigned int i = 0; i < M->n; i++){
		for(unsigned int j = 0; j < M->m; j++){
			Knapsack(i, j, M, ex_p);
		}
	}
}

int main(){
	WeightBenefits* ex_p = new WeightBenefits();
	Matrix<unsigned int>* M = new Matrix<unsigned int>(ex_p->n + 1, ex_p->W + 1);


	clock_t begin = clock();	

	fillKnapsack(M, ex_p);

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	std::cout<<"El tiempo de ejecucion: "<<(elapsed_secs*1000)<<"ms \n";

	M->print();

	delete ex_p;
	delete M;
	return EXIT_SUCCESS;
}

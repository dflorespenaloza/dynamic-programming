#include "../Framework/framework_dp_CUDA.cu"
#include "profile_time.h"

class WeightBenefits{
	public:
		static const unsigned int n = 4095;
		static const unsigned int W = 299999;

		unsigned int w[n];
		unsigned int p[n];

		WeightBenefits(){
			/*w[0]=5;
			w[1]=4;
			w[2]=6;
			w[3]=3;

			p[0]=10;
			p[1]=40;
			p[2]=30;
			p[3]=50;*/

			for(unsigned int i=0; i < 4095; i++){
				w[i] = rand()%1000;
				p[i] = rand()%1000;
			}
		}
};

template <typename T, class U>
__device__ void knapsack(unsigned int i, unsigned int j, Matrix<T>* M, U* ex_p){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		unsigned int wi1 = ex_p->w[i-1];
		unsigned int mi1 = M->getAt(i-1, j);
		if(wi1 > j){
			M->setAt(i, j, mi1);
		}else{
			M->setAt(i, j, max(mi1, M->getAt(i-1, j-wi1) + ex_p->p[i-1]));
		}	
	}
}

__device__ void (*fillKnapsack)(unsigned int, unsigned int, Matrix<unsigned int>*, WeightBenefits*) = knapsack;


//PARA EJECUTARLO EN CPU
void knapsackCPU(unsigned int i, unsigned int j, Matrix<unsigned int>* M, WeightBenefits* ex_p){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		unsigned int wi1 = ex_p->w[i-1];
		unsigned int mi1 = M->getAt(i-1, j);
		if(wi1 > j){
			M->setAt(i, j, mi1);
		}else{
			M->setAt(i, j, max(mi1, M->getAt(i-1, j-wi1) + ex_p->p[i-1]));
		}
	}
}

void fillKnapsackCPU(Matrix<unsigned int>* M, WeightBenefits* ex_p){
	for(unsigned int i = 0; i < M->n; i++){
		for(unsigned int j = 0; j < M->m; j++){
			knapsackCPU(i, j, M, ex_p);
		}
	}
}

//PARA COMPROBAR SI LAS DOS MATRICES SON IGUALES.
bool equal(Matrix<unsigned int>* M_CPU, Matrix<unsigned int>* M_GPU){
	for(unsigned int i=0; i<M_CPU->n; i++){
		for(unsigned int j=0; j<M_GPU->m; j++){
			if(M_CPU->getAt(i,j) != M_GPU->getAt(i,j)){
				return false;
			}
		}
	}
	return true;
}

int main(){

	WeightBenefits* ex_p = new WeightBenefits();
	Matrix<unsigned int>* M_CPU = new Matrix<unsigned int>(ex_p->n + 1, ex_p->W + 1);


	struct rusage ru_begin;
        struct rusage ru_end;
        struct timeval tv_elapsed;
        getrusage(RUSAGE_SELF, &ru_begin);

	fillKnapsackCPU(M_CPU, ex_p);

	getrusage(RUSAGE_SELF, &ru_end);
        timeval_subtract(&tv_elapsed, &ru_end.ru_utime, &ru_begin.ru_utime);
	debugLog("En cpu tardo %g ms\n", (tv_elapsed.tv_sec +(tv_elapsed.tv_usec/1000000.0))*1000.0);

	//M_CPU->print();

	Matrix<unsigned int>* M = new Matrix<unsigned int>(ex_p->n + 1, ex_p->W + 1);
	//Voy a medir tiempo
	cudaFree(0);
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	dp_by_rows(&fillKnapsack, M, ex_p);
	//Imprimo tiempo
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	float time;
	cudaEventElapsedTime(&time, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	printf("Tiempo de ejecucion en GPU es %f ms\n", time);	
	printf("Las matrices son iguales: %d \n",equal(M_CPU, M) );
	printf("El valor óptimo es:%d\n", M_CPU->getAt(ex_p->n,ex_p->W));

	//M->print();

	delete M_CPU;
	delete ex_p;
	delete M;
	return EXIT_SUCCESS;
}

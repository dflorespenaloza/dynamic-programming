#include <iostream>
#include <stdio.h>
#include "../Framework/log.cu"
#include "../Framework/Matrix.cu"
unsigned int TPB =128;

class WeightBenefits{
	public:
		static const unsigned int n = 4;//4095;
		static const unsigned int W = 10;//299999;

		unsigned int w[n];
		unsigned int p[n];
};

__constant__ WeightBenefits ex_p;

__device__ void knapsack(unsigned int i, unsigned int j, Matrix<unsigned int>* M){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		unsigned int wi1 = ex_p.w[i-1];
		unsigned int mi1 = M->getAt(i-1, j);
		if(wi1 > j){
			M->setAt(i, j, mi1);
		}else{
			M->setAt(i, j, max(mi1, M->getAt(i-1, j-wi1) + ex_p.p[i-1]));
		}	
	}
}

void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){
		printf("CUDA error: %s", cudaGetErrorString(error));
		exit(EXIT_FAILURE);
	}
}

__global__ void dp_by_rows_stage(Matrix<unsigned int>* M_device, int row){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if( id < M_device->m ){
		knapsack(row, id, M_device);
	}
}


template <typename T>
void dp_by_rows(Matrix<T>* M_host){
	//Guardo la direccion del arreglo del Host
	T* arr_M_host = M_host->M;

	//Asigno memoria para la Matrix en el Device
	Matrix<T>* M_device;
	cudaMalloc(&M_device, sizeof(Matrix<T>));

	//Asigno memoria para el arreglo en el Device.
	T* arr_M_device;
	cudaMalloc(&arr_M_device, (M_host->n*M_host->m)*sizeof(T));

	//Intercambio la direccion del arreglo del host por la direccion en el Device.
	M_host->M = arr_M_device;

	//Copio la Matrix del Host al Device. NOTA: NO EL CONTENIDO DE LA MATRIX SOLO LOS APUNTADORES
	cudaMemcpy(M_device, M_host, sizeof(Matrix<T>), cudaMemcpyHostToDevice);


	//Copio los parametros del Host al Device
	unsigned int* random = new unsigned int[2*ex_p.n];
	for(unsigned int i=0; i < 2*ex_p.n; i++){
		random[i] = rand()%1000;
	}

	/*random[0]=5;
	random[1]=4;
	random[2]=6;
	random[3]=3;
	random[4]=10;
	random[5]=40;
	random[6]=30;
	random[7]=50;*/

	cudaMemcpyToSymbol(ex_p, random, (2*ex_p.n)*sizeof(unsigned int));
	//cudaMemcpy(ex_p, ex_p_host, sizeof(U), cudaMemcpyHostToDevice);


//#################################LANZANDO EL KERNEL
	//Preparando cosas previas antes de mandar a llamar al KERNEL
	int nb = (int) ceil(M_host->m/((float)TPB));

	for(int row=0; row < M_host->n; row++){
		dp_by_rows_stage<<<nb, TPB>>>(M_device, row);
		unsigned int step = row*(M_host->m);
		cudaMemcpyAsync(arr_M_host+step, arr_M_device+step, M_host->m*sizeof(T), cudaMemcpyDeviceToHost);
	}


//###################################SINCRONIZANDO Y LIBERANDO
	//Sincronizando y verificando que no hubo errores durante la ejecucion.
	cudaDeviceSynchronize();
	cudaCheckForError();

	M_host->M = arr_M_host;

	//Liberando el arreglo del Device
	cudaFree(arr_M_device);

	//Liberando la Matriz del Device.
	cudaFree(M_device);
}


int main(){

	//WeightBenefits* ex_p = new WeightBenefits();
	Matrix<unsigned int>* M = new Matrix<unsigned int>(ex_p.n + 1, ex_p.W + 1);

	//Voy a medir tiempo
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	dp_by_rows(M);

	//Imprimo tiempo
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	float time;
	cudaEventElapsedTime(&time, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	printf("Tiempo de ejecucion en GPU es %f ms\n", time);	

	M->print();

	delete M;

	return EXIT_SUCCESS;
}

#include<cuda.h>
#include <stdio.h>
#include <assert.h>

#define myCudaAssert(exp) if((e = exp) != cudaSuccess) {printf("%s : line %d", cudaGetErrorString(e), __LINE__); assert(0);}
#define MCA myCudaAssert
#define myMin(a,b) ((a) < (b) ? (a) : (b))
#define myMax(a,b) ((a) > (b) ? (a) : (b))
#define myAbs(a) ((a) < 0 ? (-a): a)

#define N 299999
#define MaxW 4095
#define TPB 128
#define NB ((((MaxW+1)+TPB-1)))/TPB 
int V[N];
int W[N];


//lsbs last stage block size
__global__ void kpsckStage(int* OPT, int * w, int * v, int maxw,int stage){

	int itid = threadIdx.x;
	int bid = blockIdx.x;
	int tid = blockDim.x * bid + itid; 
	int stride = blockDim.x * gridDim.x;
	while(tid<maxw+1){	
		
		if(stage == 0){
		 OPT[tid]=0;
		}
		else{

			if(w[stage-1] > tid){
				OPT[stage*(maxw+1) + tid] = OPT[(stage-1)*(maxw+1) + tid];
			}
			else{
				int temp1= OPT[(stage-1)*(maxw+1)+tid];
				int temp2 = OPT[(stage-1)*(maxw+1)+tid-w[stage-1]]+v[stage-1];
				OPT[stage*(maxw+1) + tid] = (temp1 > temp2)? temp1 : temp2;
			}
}
		tid += stride;
			
	
	}

}



int main(void){
	cudaError_t e;
	

  


	// creo apuntadores para los enteros en device
	int * T_d;
	int * V_d;
	int * W_d;


	// creamos los datos de entrada en el Host
	
	

	for(unsigned int i=0; i < 4095; i++){
                       W[i] = rand()%1000;
                       V[i] = rand()%1000;
         }


	myCudaAssert(cudaMalloc(&T_d, (MaxW+1)*(N+1)*sizeof(int)));
	myCudaAssert(cudaMalloc(&V_d, N*sizeof(int)));
	myCudaAssert(cudaMalloc(&W_d, N*sizeof(int)));

	// copio los datos de entrada hacia device

	myCudaAssert(cudaMemcpy(V_d, V, N*sizeof(int), cudaMemcpyHostToDevice));
	myCudaAssert(cudaMemcpy(W_d, W, N*sizeof(int), cudaMemcpyHostToDevice));


	// inicializamos eventos

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// se lanza kernel y se checan errores
		cudaEventRecord(start,0);
	
	for(int diag = 0; diag < N+1; diag++){

		kpsckStage<<<NB,TPB>>>(T_d, W_d, V_d,MaxW,diag);
	}
		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		myCudaAssert(cudaGetLastError());

		float elapsedTime;
		cudaEventElapsedTime(&elapsedTime, start, stop);	

		printf("Los kernels tardaron %g ms\n", elapsedTime);
	// se copian resultados de device a host
	int result= 0;

	myCudaAssert(cudaMemcpy(&result, &(T_d[(N+1)*(MaxW+1)-1]), sizeof(int), cudaMemcpyDeviceToHost));

	
	printf("Optimal Value of Stolen Items:%d\n", result);

	return 0;
}

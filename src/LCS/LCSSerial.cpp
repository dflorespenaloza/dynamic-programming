#include "../dp_Serial.h"
#include <string.h>

class Cadenas{
	public:
		char X[13];
		char Y[14]; 
		Cadenas(const char X[13], const char Y[14]){
			memcpy((void*)this->X,(void*)X, 13*sizeof(char));
			memcpy((void*)this->Y,(void*)Y, 14*sizeof(char));
		}
};

void LCS(unsigned int i, unsigned int j, Matrix<unsigned int>* M, Cadenas* ex_p){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		if(ex_p->X[i - 1] == ex_p->Y[j - 1] ){
			M->setAt(i, j, 1 + M->getAt(i -1, j -1));
		}else{
			M->setAt(i, j, max(M->getAt(i - 1, j), M->getAt(i, j - 1)) );
		}
	}
}

void fillLCS(Matrix<unsigned int>* M, Cadenas* ex_p){
	for(unsigned int i = 0; i < M->n; i++){
		for(unsigned int j = 0; j < M->m; j++){
			LCS(i, j, M, ex_p);
		}
	}
}

string recoveryLCS(unsigned int i, unsigned j, Matrix<unsigned int>* M, Cadenas* ex_p){
	if(i == 0 || j == 0){
		return "";
	}else{
		if( ex_p->X[i - 1] == ex_p->Y[j - 1] ){
			return recoveryLCS(i-1, j-1, M, ex_p) + ex_p->X[i - 1];
		}else{
			if(M->getAt(i, j-1) > M->getAt(i-1, j) ){
				return recoveryLCS(i, j-1, M, ex_p);
			}else{
				return recoveryLCS(i-1, j, M, ex_p);
			}
		}
	}
}

int main(){
	const unsigned int n = 13;
	const unsigned int m = 14;
	Matrix<unsigned int> M (n + 1, m + 1);

	Cadenas ex_p ("gacacgggattag","ggacatcggaatag");

	fillLCS(&M, &ex_p);

	//M.print(); //solucion gacacggatag	
	cout << "La LCS es: " << recoveryLCS(M.n, M.m, &M, &ex_p) << endl;
	
	return EXIT_SUCCESS;
}

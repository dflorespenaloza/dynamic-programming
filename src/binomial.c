#include <iostream>
#include <cstdlib>
using namespace std;

typedef unsigned int uint;

uint binomial(int n, int k){
// robust computation of n choose k
// DAVID,MANUEL: se les ocurre algo mejor?
  double r(1);
  double i;
  if (n-k < k) 
    for (i=1;i<=n-k;i++)
           r = r*(1+k/i);
  else for (i=1;i<=k;i++)
           r = r*(1+(n-k)/i);
  return uint(r+0.5);
}

void kSubsetUnrank(uint r, int k, int n){
// compute the k-subset of {1,,...,n} of rank r
int i, x(1);
uint temp;
for (i=1; i<=k; i++){
   while ((temp=binomial(n-x, k-i)) <= r){
     r = r - temp;
     x++;
   }
   cout << x << ' ';
   x++;
}
}

int main(int argc, char** argv) {
  int n(5), k(3);
  uint r; // rank of a subset
  uint m; //# of subsets of 1..n of size k
  if (argc > 1) n = atoi(argv[1]);
  if (argc > 2) k = atoi(argv[2]);

  m = binomial(n,k);
  cout<<"There are "<<m<<' '<<k<<"-subsets of 1.." <<n<<endl;

  for(r=0;r<m;r++){
     cout << r << ": ";
     kSubsetUnrank(r, k, n); cout << endl;
  }
}

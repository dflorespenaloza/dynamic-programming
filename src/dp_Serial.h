#include <iostream>
#include <cstdlib>

using namespace std;

template <typename T>
class Matrix{
	private:
		T* M;

		int getIndex(unsigned int i, unsigned int j){
			return i*m + j;
		}
	public:
		unsigned int n;
		unsigned int m;
	
		Matrix(const unsigned int n, const unsigned int m){
			this->n = n;
			this->m = m;
			this->M = (T*) malloc( (n*m)*sizeof(T));
		}

		void setAt(unsigned int i, unsigned int j, T value){
			M[ getIndex(i,j) ] = value;
		}

		T getAt(unsigned int i, unsigned int j){
			return M[ getIndex(i,j) ];
		}

		void print(){
			for(unsigned int index = 0; index < n*m; index++){
				cout << M[ index ] << " ";
				if( (index+1) % m == 0){
					cout << endl;
				}
			}
		}
};

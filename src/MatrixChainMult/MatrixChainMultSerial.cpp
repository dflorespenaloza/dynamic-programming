#include "../dp_Serial.h"

//########################################################
//  MATRIX CHAIN CON DATOS DE ESCRITURA EN LOS PARAMETROS
/*
class Dimensions{
	public:
		unsigned int p[7];
		unsigned int s[36];

		Dimensions(){
			p[0] = 30;
			p[1] = 35;
			p[2] = 15;
			p[3] = 5;
			p[4] = 10;
			p[5] = 20;
			p[6] = 25;
		}
};

void MatrixChainMult(unsigned int i, unsigned int j, Matrix<unsigned int>* M, Dimensions* ex_p){
	if(i == j){
		M->setAt(i, j, 0);
	}else{
		unsigned minimun = 0xffffffff;
		for(unsigned int k = i; k < j; k++){
			unsigned int aux = M->getAt(i, k) + M->getAt(k + 1, j) + ex_p->p[i]*ex_p->p[k + 1]*ex_p->p[j + 1];
			if(aux < minimun){
				minimun = aux;
				ex_p->s[i*M->m + j] = k;
			}
		}
		M->setAt(i, j, minimun);
	}
}

void fillMatrixChainMult(Matrix<unsigned int>* M, Dimensions* ex_p){
	for(unsigned int diag = 0; diag < M->n; diag++){
		for(unsigned int id = 0; id < M->n - diag; id++){
			MatrixChainMult(id, id + diag, M, ex_p);
		}
	}
}

void print_parens(unsigned int i, unsigned int j, unsigned int* s, int n){
	if (i == j){
		cout<<"A"<<i;
	}else{
		cout << "(";
		print_parens(i, s[i*n + j], s, n);
		print_parens(s[i*n + j] + 1, j, s, n);
		cout << ")";
	}
}


int main(){
	const unsigned int n = 6;
	Matrix<unsigned int> M (n, n);
	Dimensions ex_p;
		
	fillMatrixChainMult(&M, &ex_p);

	//M.print();//resultado 15125 ((A0(A1A2))((A3A4)A5))
	cout<<"La forma de parentizar las matrices es: ";
	print_parens(0, n - 1, ex_p.s, n);
	cout << endl;

	return EXIT_SUCCESS;
}*/


//########################################################
//  MATRIX CHAIN CON DATOS DE ESCRITURA EN OBJETOS DENTRO DE LA MATRIZ

class Dimensions{
	public:
		unsigned int p[7];

		Dimensions(){
			p[0] = 30;
			p[1] = 35;
			p[2] = 15;
			p[3] = 5;
			p[4] = 10;
			p[5] = 20;
			p[6] = 25;
		}
};

class Point{
	public:
		unsigned int v;
		unsigned int s;
		Point(){
			v = 0;
			s = 0;
		}

		Point(unsigned int v, unsigned int s){
			this->v = v;
			this->s = s;
		}
};

ostream& operator<< (ostream &out, Point &cPoint){
	out << "(" << cPoint.v << ", " << cPoint.s << ")";
	return out;
}

void MatrixChainMult(unsigned int i, unsigned int j, Matrix<Point>* M, Dimensions* ex_p){
	if(i == j){
		Point p;
		M->setAt(i, j, p);
	}else{
		Point p (0xffffffff, 0);
		for(unsigned int k = i; k < j; k++){
		    unsigned int aux = M->getAt(i, k).v + M->getAt(k + 1, j).v + ex_p->p[i]*ex_p->p[k + 1]*ex_p->p[j + 1];
			if(aux < p.v){
				p.v = aux;
				p.s = k;
			}
		}
		M->setAt(i, j, p);
	}
}

void fillMatrixChainMult(Matrix<Point>* M, Dimensions* ex_p){
	for(unsigned int diag = 0; diag < M->n; diag++){
		for(unsigned int id = 0; id < M->n - diag; id++){
			MatrixChainMult(id, id + diag, M, ex_p);
		}
	}
}

void print_parens(unsigned int i, unsigned int j, Matrix<Point>* M){
	if (i == j){
		cout<<"A"<<i;
	}else{
		cout << "(";
		print_parens(i, M->getAt(i, j).s, M);
		print_parens(M->getAt(i,j).s + 1, j, M);
		cout << ")";
	}
}


int main(){
	const unsigned int n = 6;
	Matrix<Point> M (n, n);
	Dimensions ex_p;

	fillMatrixChainMult(&M, &ex_p);

	//M.print();//resultado 15125 ((A0(A1A2))((A3A4)A5))

	cout<<"La forma de parentizar las matrices es: ";
	print_parens(0, n - 1, &M);
	cout << endl;

	return EXIT_SUCCESS;
}

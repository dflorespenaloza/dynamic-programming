#include "../dp_Serial.h"

void BinCoef(unsigned int i, unsigned int j, Matrix<unsigned int>* M){
	if( j == 0 || i == j ){
		M->setAt(i, j, 1);
	}else if(j < i){
		M->setAt(i, j, M->getAt(i-1, j-1) + M->getAt(i - 1, j));	
	}else{
		M->setAt(i, j, 0);
	}
}

void fillBinCoef(Matrix<unsigned int>* M){
	for(unsigned int i = 0; i < M->n; i++){
		for(unsigned int j = 0; j <= i; j++){
			BinCoef(i, j, M);
		}
	}
}

int main(){
	const unsigned int n = 16;
	const unsigned int m = 11;
	Matrix<unsigned int> M(n + 1,m + 1);

	fillBinCoef(&M);

	//M.print(); //resultado 4368
	cout << "(" << n << "," << m << ") es: " << M.getAt(n, m) << endl;
	
	return EXIT_SUCCESS;
}

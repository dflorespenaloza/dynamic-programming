#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

/*template <typename T>
class Matrix{
	public:
		int n;
		int m;
		T w;
		Matrix(int n, int m, T x){
			this->n = n;
			this->m = m;
			w=x;
		}
};

class NULL_EX{
	public:
		int n;
		NULL_EX(){
			n=100;
		}
};

template <typename T, class U, typename Z>
__global__ void dp_stage(Matrix<T>* M_dev, U* ex_p_dev, Z* dev_func){
	(dev_func[0])(99, 99, M_dev, ex_p_dev);
}

void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){
		cout << "CUDA error: " << cudaGetErrorString(error) << endl;
		exit(EXIT_FAILURE);
	}
}

template <typename T, class U>
void dp_by_something(Matrix<T>* M_host, U* ex_p_host, const void* f_name){
	//Defino el tipo de la funcion
	//typedef typename funcType<T, U>::func f; //No se si este bien
	typedef void (*f)(unsigned int, unsigned int, Matrix<T>*, U*);

	//Funcion en el Host
	f* host_func = (f*)malloc(sizeof(f));

	//Funcion en el Device
	f* dev_func;
	cudaMalloc((void**)&dev_func,sizeof(f));

	//PARA CUDA 5 SE TENDRIA QUE QUITAR EL CAST
	cudaMemcpyFromSymbol(&host_func[0], (const char*)f_name, sizeof(f));
	cudaMemcpy(dev_func, host_func, sizeof(f), cudaMemcpyHostToDevice);

	free(host_func);

	//#ESTO ES PURA PAJA LO IMPORTANTE ES LO DE ARRIBA
	Matrix<T>* dev_M;
	cudaMalloc((void**)&dev_M, sizeof(Matrix<T>));
	cudaMemcpy(dev_M, M_host, sizeof(Matrix<T>), cudaMemcpyHostToDevice);

	U* ex_p_dev;
	cudaMalloc((void**)&ex_p_dev,sizeof(U));
	cudaMemcpy(ex_p_dev, ex_p_host, sizeof(U), cudaMemcpyHostToDevice);


	dp_stage<<<1,1>>>(dev_M, ex_p_dev, dev_func);

	cudaThreadSynchronize();
	cudaCheckForError();
}

//#################ESTO ES LO QUE TENDRIA QUE DECLARAR EL USUARIO
template <typename T, class U>
__device__ void my_function1(unsigned int i, unsigned int j, Matrix<T>* M, U* ex_p){
	printf("%d %d \n", i, j);
	printf("%d %d %f\n", M->n, M->m, M->w );
	printf("%d \n", ex_p->n);
	printf("UNO");
}

__device__ void (*pfunc1)(unsigned int, unsigned int, Matrix<float>*, NULL_EX*) = my_function1;


int main()
{
	Matrix<float> M (3,3, 2.1);
	NULL_EX ex_p;

	dp_by_something(&M, &ex_p, (const void*)"pfunc1");

	return EXIT_SUCCESS;
}*/

void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){

		cout << "CUDA error: " << cudaGetErrorString(error) << endl;
		exit(EXIT_FAILURE);
	}
}

__device__ void miFuncion(unsigned int i){
	printf("Hello\n");	
}

__device__ void (*func)(unsigned int) = miFuncion;


template <typename Z>
__global__ void dp_stage(Z* dev_f){
	(dev_f[0])(2);
}

template <typename T>
void dp_by_something(T* f_name){
	//typedef void (*f_type)(unsigned int);

	//Funcion en el Host
	T* host_f = (T*)malloc(sizeof(T));

	//Funcion en el Device
	T* dev_f;
	cudaMalloc((void**)&dev_f, sizeof(T));

	cudaMemcpyFromSymbol(&host_f[0], *f_name, sizeof(T));
	cudaMemcpy(dev_f, host_f, sizeof(T), cudaMemcpyHostToDevice);

	free(host_f);

	dp_stage<<<1,1>>>(dev_f);

	cudaThreadSynchronize();
	cudaCheckForError();
	cudaFree(dev_f);
}



int main()
{
	dp_by_something(&func);
	return EXIT_SUCCESS;
}


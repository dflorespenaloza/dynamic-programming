#include "../framework_dp_CUDA.cu"

template <typename T, class U>
__device__ void BinCoef(unsigned int i, unsigned int j, Matrix<T>* M, U* ex_p){
	if( j == 0 || i == j ){
		M->setAt(i, j, 1);
	}else if(j < i){
		M->setAt(i, j, M->getAt(i-1, j-1) + M->getAt(i - 1, j) );	
	}else{
		M->setAt(i, j, 0);
	}
}

__device__ void (*fillBinCoef)(unsigned int, unsigned int, Matrix<unsigned int>*, NULL_EX_P*) = BinCoef;


int main(){
	const unsigned int n = 16;
	const unsigned int m = 11;

	Matrix<unsigned int> M(n + 1,m + 1);

	dp_by_rows(&fillBinCoef, &M, &null_ex_p);

	M.print(); //resultado 4368
	//cout << "(" << n << "," << m << ") es: " << M.getAt(n,m) << endl;

	return EXIT_SUCCESS;
}

template <typename T>
class Matrix{
	public:
		T* M;
		unsigned int n;
		unsigned int m;
	
		Matrix(const unsigned int n, const unsigned int m){
			this->n = n;
			this->m = m;
			cudaMallocHost((void**)&M, n*m*sizeof(T));
			//this->M = new T[n*m];
		}

		__device__ __host__ void setAt(unsigned int i, unsigned int j, T value){
			M[ getIndex(i,j) ] = value;
		}

		__device__ __host__ T getAt(unsigned int i, unsigned int j){
			return M[ getIndex(i,j) ];
		}

		__device__ __host__ int getIndex(unsigned int i, unsigned int j){
			return i*m + j;
		}

		__host__ __device__ unsigned int numActive(unsigned int diagonal){
			if(diagonal < min(m,n)){
				return diagonal + 1;
			}else if(diagonal < max(m,n) ){
				return min(n,m);
			}else{
				return m + (n - 1) -diagonal;
			}
		}

		unsigned int numTotalDiags(){
			return m + (n - 1);
		}

		void print(){
			for(unsigned int index = 0; index < n*m; index++){
				debugLog("%d ",M[ index ]);
				if( (index+1) % m == 0){
					debugLog("\n");
				}
			}
		}

		~Matrix(){
			cudaFreeHost(M);
			//delete M;
		}
};

#ifdef  NDEBUG
#define debugLog(...) ()
#else
#define debugLog(...) printf(__VA_ARGS__)
#endif

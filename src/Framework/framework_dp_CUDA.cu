#include "framework_dp_CUDA.h"

//###########################################################3
//                 EMPIEZA EL FRAMEWORK

void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){
		debugLog("CUDA error: %s", cudaGetErrorString(error));
		exit(EXIT_FAILURE);
	}
}


template <class U>
U* manage_ExtraParams(U* ex_p_host){
	//Asigno memoria en el Device para los parametros.
	U* ex_p_device;
	cudaMalloc((void**)&ex_p_device, sizeof(U));

	//Copio los parametros del Host al Device
	cudaMemcpy((void*)ex_p_device, (void*)ex_p_host, sizeof(U), cudaMemcpyHostToDevice);

	//regreso el apuntador de los parametos en el Device
	return ex_p_device;
}


template <typename F_TYPE, typename T, class U>
Vars<F_TYPE, T, U>* setUpVars(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host){
	Vars<F_TYPE, T, U>* vars = new Vars<F_TYPE, T, U>();
	//Guardo la direccion del arreglo del Host
	vars->arr_M_host = M_host->M;

	//Asigno memoria para la Matrix en el Device
	cudaMalloc((void**) &(vars->M_device), sizeof(Matrix<T>));

	//Asigno memoria para el arreglo en el Device.
	cudaMalloc((void**) &(vars->arr_M_device), (M_host->n*M_host->m)*sizeof(T));

	//Intercambio la direccion del arreglo del host por la direccion en el Device.
	M_host->M = vars->arr_M_device;

	//Copio la Matrix del Host al Device. NOTA: NO EL CONTENIDO DE LA MATRIX SOLO LOS APUNTADORES
	cudaMemcpy(vars->M_device, M_host, sizeof(Matrix<T>), cudaMemcpyHostToDevice);

	//Asignando memoria y copiando los parametros extras. 
	vars->ex_p_device = manage_ExtraParams(ex_p_host);

	//Asignando memoria para la funcion en host	
	F_TYPE* host_f = new F_TYPE();

	//Asigno memoria para la funcion en Device
	cudaMalloc((void**)&(vars->dev_f),sizeof(F_TYPE));

	//Copio el simbolo de la funcion en el Device al Host
	cudaMemcpyFromSymbol(&host_f[0], *f, sizeof(F_TYPE));

	//Copio la el simbolo al Device
	cudaMemcpy(vars->dev_f, host_f, sizeof(F_TYPE), cudaMemcpyHostToDevice);

	//Libero la memoria de la funcion en Host
	delete host_f;
	return vars;
}


template <typename F_TYPE, typename T, class U>
void synchCopyFree(Matrix<T>* M_host, U* ex_p_host, Vars<F_TYPE, T, U>* vars, bool copyBack_ex_p){
	//Sincronizando y verificando que no hubo errores durante la ejecucion.
	cudaDeviceSynchronize();
	cudaCheckForError();

	//Copiando el arreglo de la Matriz que se lleno en el Device de vuelta al Host.
//cudaMemcpy((void*)vars->arr_M_host, (void*) vars->arr_M_device, (M_host->n*M_host->m)*sizeof(T),cudaMemcpyDeviceToHost);
	M_host->M = vars->arr_M_host;

	//Copiando la Estructura de vuelta al Host.
	if(copyBack_ex_p){
		cudaMemcpy((void*)ex_p_host, (void*) vars->ex_p_device, sizeof(U), cudaMemcpyDeviceToHost);
	}

	//Liberando el arreglo del Device
	cudaFree(vars->arr_M_device);

	//Liberando la Matriz del Device.
	cudaFree(vars->M_device);

	//Liberando la estructura del Device.
	cudaFree(vars->ex_p_device);

	//Liberando la funcion el el Device
	cudaFree(vars->dev_f);

	//Liberando variables
	delete vars;
}



//##################################################################################3
//                         EMPIEZAN LOS PATRONES DE ITERACION
//POR DIAGONALES DE NOROESTE A SURESTE
template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, int diagInit, int diagEnd, bool copyBack_ex_p){	
	//Estableciendo las variables principales
	Vars<F_TYPE, T, U>* vars = setUpVars(f, M_host, ex_p_host);

	//Preparando cosas previas antes de mandar a llamar al KERNEL
	bool isGrowing = 0 <= (diagEnd - diagInit);
	for(int diagonal=diagInit; (isGrowing)?(diagonal<=diagEnd):(diagEnd<=diagonal); diagonal+=(isGrowing)?1:-1){
		int nactive = M_host->numActive(diagonal);
		int nb = (int)ceil(nactive/((float)TPB));
		int nt = (int)ceil(nactive/((float)nb));
		//Mandando a llamar al Kernel.
		dp_by_diagonals_NO_SE_stage<<<nb, nt>>>(vars->dev_f, vars->M_device, vars->ex_p_device, diagonal);
	}

	//Sincronizando los Threads, copiando la matriz y estructura de vuelta al host y liberando memoria.
	synchCopyFree(M_host, ex_p_host, vars, copyBack_ex_p);
}

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, bool copyBack_ex_p){	
	dp_by_diagonals_NO_SE(f, M_host, ex_p_host, 0, M_host->numTotalDiags() - 1, copyBack_ex_p);
}

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host){	
	dp_by_diagonals_NO_SE(f, M_host, ex_p_host, false);
}

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_diagonals_NO_SE_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int diagonal){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if(id < M_device->numActive(diagonal)){
		if(diagonal < M_device->m){
			(dev_f[0])(id, diagonal - id, M_device, ex_p_device);
		}else{
			(dev_f[0])(diagonal - (M_device->m - 1) + id, (M_device->m - 1) - id, M_device, ex_p_device);
		}
	}
}


//POR DIAGONALES DE SUROESTE A NORESTE
template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, int diagInit, int diagEnd, bool copyBack_ex_p){	
	//Estableciendo las variables principales
	Vars<F_TYPE, T,U>* vars = setUpVars(f, M_host, ex_p_host);

	//Preparando cosas previas antes de mandar a llamar al KERNEL
	bool isGrowing = 0 <= (diagEnd - diagInit);
	for(int diagonal=diagInit; (isGrowing)?(diagonal<=diagEnd):(diagEnd<=diagonal); diagonal+=(isGrowing)?1:-1){
		int nactive = M_host->numActive(diagonal);
		int nb = (int)ceil(nactive/((float)TPB));
		int nt = (int)ceil(nactive/((float)nb));
	     dp_by_diagonals_SO_NE_stage<<<nb, nt>>>(vars->dev_f, vars->M_device, vars->ex_p_device, diagonal);		
	}

	//Sincronizando los Threads, copiando la matriz y estructura de vuelta al host y liberando memoria.
	synchCopyFree(M_host, ex_p_host, vars, copyBack_ex_p);
}

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, bool copyBack_ex_p){	
	dp_by_diagonals_SO_NE(f, M_host, ex_p_host, 0, M_host->numTotalDiags() - 1, copyBack_ex_p);		
}

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host){	
	dp_by_diagonals_SO_NE(f, M_host, ex_p_host, false);		
}


template <typename F_TYPE, typename T, class U>
__global__ void dp_by_diagonals_SO_NE_stage(F_TYPE dev_f, Matrix<T>* M, U* ex_p, int diagonal){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if( id < M->numActive(diagonal) ){
		if(diagonal < M->n){
			(dev_f[0])((M->n - 1) - diagonal + id, id, M, ex_p);
		}else{
			(dev_f[0])(id, (diagonal - (M->n - 1)) + id, M, ex_p);
		}
	}
}


//PATRON DE ITERACION POR FILAS
template <typename F_TYPE, typename T, class U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, int rowInit, int rowEnd, bool copyBack_ex_p){

	typedef void (*fun)(F_TYPE*, Matrix<T>*,U*,int);

	fun p = dp_by_rows_stage;

	struct cudaFuncAttributes attr;
	cudaFuncGetAttributes(&attr, p);
	printf("VALOR REG: %d \n", attr.numRegs);

	//Estableciendo las variables principales
	Vars<F_TYPE, T, U>* vars = setUpVars(f, M_host, ex_p_host);

	//Preparando cosas previas antes de mandar a llamar al KERNEL
	bool isGrowing = 0 <= (rowEnd - rowInit);
	int nb = (int) ceil(M_host->m/((float)TPB));
	//int nt = (int) ceil(M_host->m/((float)nb));

	for(int row=rowInit; (isGrowing)?(row<=rowEnd):(rowEnd<=row); row+=(isGrowing)?1:-1){
		dp_by_rows_stage<<<nb, TPB>>>(vars->dev_f, vars->M_device, vars->ex_p_device, row);
		unsigned int step = row*(M_host->m);
	cudaMemcpyAsync(vars->arr_M_host+step, vars->arr_M_device+step, M_host->m*sizeof(T), cudaMemcpyDeviceToHost);
	}

	//Sincronizando los Threads, copiando la matriz y estructura de vuelta al host y liberando memoria.
	synchCopyFree(M_host, ex_p_host, vars, copyBack_ex_p);
}


template <typename F_TYPE, typename T, class U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, bool copyBack_ex_p){
	dp_by_rows(f, M_host, ex_p_host, 0, M_host->n - 1, copyBack_ex_p);
}

template <typename F_TYPE, typename T, typename U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host){
	dp_by_rows(f, M_host, ex_p_host, false);
}

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_rows_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int row){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if( id < M_device->m ){
		(dev_f[0])(row, id, M_device, ex_p_device);
	}
}


//PATRON DE ITERACION POR COLUMNAS
template <typename F_TYPE, typename T, class U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, int columInit, int columEnd, bool copyBack_ex_p){
	//Estableciendo las variables principales
	Vars<F_TYPE, T, U>* vars = setUpVars(f, M_host, ex_p_host);

	//Preparando cosas previas antes de mandar a llamar al KERNEL
	bool isGrowing = 0 <= (columEnd - columInit);
	int nb = (int) ceil(M_host->n/((float)TPB));
	int nt = (int) ceil(M_host->n/((float)nb));
	for(int colum=columInit; (isGrowing)?(colum<=columEnd):(columEnd<=colum); colum+=(isGrowing)?1:-1){
		dp_by_colums_stage<<<nb, nt>>>(vars->dev_f, vars->M_device, vars->ex_p_device, colum);		
	}

	//Sincronizando los Threads, copiando la matriz y estructura de vuelta al host y liberando memoria.
	synchCopyFree(M_host, ex_p_host, vars, copyBack_ex_p);	
}

template <typename F_TYPE, typename T, class U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, bool copyBack_ex_p){
	dp_by_colums(f, M_host, ex_p_host, 0, M_host->m - 1, copyBack_ex_p);
}

template <typename F_TYPE, typename T, typename U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host){
	dp_by_colums(f, M_host, ex_p_host, false);
}

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_colums_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int colum){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if( id < M_device->n ){
		(dev_f[0])(colum, id, M_device, ex_p_device);
	}
}

template <typename F_TYPE, typename T, class U>
struct Vars{
	//Direccion del arreglo del Host
	T* arr_M_host;

	//Memoria para la Matrix en el Device
	Matrix<T> *M_device;

	//Memoria para el arreglo en el Device.
	T *arr_M_device;

	//Parametros extras. 
	U* ex_p_device;

	//Funcion a ejecutar
	F_TYPE* dev_f;
};

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include<stdlib.h>
#include <syslog.h>
#include <stdio.h>

//#####################################################
//                  DEFINICIONES PRINCIPALES

using namespace std;
const int TPB = 1024;

#include "log.cu"
#include "Matrix.cu"
#include "Vars.cpp"


//#####################################################################
//                    DEFINICIONES DE FUNCIONES

void cudaCheckForError();

template <class U>
U* manage_ExtraParams(U* ex_p_host);

template <typename F_TYPE, typename T, class U>
Vars<F_TYPE, T, U>* setUpVars(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host);

template <typename F_TYPE, typename T, class U>
void synchCopyFree(Matrix<T>* M_host, U* ex_p_host, Vars<F_TYPE, T, U>* vars, bool copyBack_ex_p);


//PATRON DE ITERACION POR DIAGOLANES DE NOROESTE A SURESTE
template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, int diagInit, int diagEnd, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_NO_SE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host);

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_diagonals_NO_SE_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int diagonal);


//PATRON DE ITERACION POR DIAGONALES DE SUROESTE A NORESTE
template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, int diagInit, int diagEnd, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_diagonals_SO_NE(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host);

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_diagonals_SO_NE_stage(F_TYPE dev_f, Matrix<T>* M, U* ex_p, int diagonal);


//PATRON DE ITERACION POR RENGLONES
template <typename F_TYPE, typename T, class U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, int rowInit, int rowEnd, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, bool copyBack_ex_p);

template <typename F_TYPE, typename T, typename U>
void dp_by_rows(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host);

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_rows_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int row);


//PATRON DE ITERACION POR COLUMNAS
template <typename F_TYPE, typename T, class U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, int columInit, int columEnd, bool copyBack_ex_p);

template <typename F_TYPE, typename T, class U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host,  U* ex_p_host, bool copyBack_ex_p);

template <typename F_TYPE, typename T, typename U>
void dp_by_colums(F_TYPE* f, Matrix<T>* M_host, U* ex_p_host);

template <typename F_TYPE, typename T, class U>
__global__ void dp_by_colums_stage(F_TYPE dev_f, Matrix<T>* M_device, U* ex_p_device, int colum);

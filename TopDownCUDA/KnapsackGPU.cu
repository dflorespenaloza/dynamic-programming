#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <vector>
#include <cmath>

class WeightBenefits{
	public:
		unsigned int n;
		unsigned int W; 
		unsigned int b[4];
		unsigned int w[4];

		WeightBenefits(){
			n = 4;
			W = 10;
			b[0] = 10; 
			w[0] = 5;
			b[1] = 40;
			w[1] = 4;
			b[2] = 30;
			w[2] = 6;
			b[3] = 50;
			w[3] = 3;
		}

		int getInicial(){
			return (n-1)*(W+1) + W;
		}


		__device__ int* getSucessors(unsigned int key){
			unsigned int i = key/(W+1);
			unsigned int j = key%(W+1);
			if(j<w[i] || i==0){
				return NULL;
			}else{
				int* sucesores = new int[2];
				sucesores[0] = (i-1)*(W+1) + j;
				sucesores[1] = (i-1)*(W+1) + j-w[i];
				return sucesores;
			}
		}

};

/*
__global__ void stage_top_down(int* tree, WeightBenefits* ex_p, unsigned int nivel){
	unsigned int id = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int nactive = (unsigned int)pow((double)2, (double)nivel) - 1;
	if(id <= nactive){
		unsigned int key = tree[nactive+id];
		if(key!=0xffffff){
			int* sucesores = ex_p->getSucessors(key);
			unsigned int pos_next = (unsigned int)pow((double)2, (double)(nivel+1))-1;
			unsigned int pos = pos_next+2*id;
			if(sucesores!=NULL){
				tree[pos] = sucesores[0];
				tree[pos+1] = sucesores[1];
			}else{
				tree[nactive+id]*=-1;
				tree[pos] = 0xffffff;
				tree[pos+1] = 0xffffff;
			}
			delete sucesores;
		}
	}	
}*/

/*
__global__ void stage_down_top(unsigned int* tree, WeightBenefits* ex_p, unsigned int nivel){
	unsigned int id = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int nactive = (unsigned int)pow((double)2, (double)nivel) - 1;
	if(id <= nactive){	
		unsigned int key = tree[nactive+id];
		if(key!=0xffffff){
			tree[nactive+id]
		}
	}
}*/


/*
void top_down(WeightBenefits* ex_p_host){
	unsigned int k = ex_p_host->n;//Niveles del arbol
	unsigned int b = 2; //Hijos por Nodo
	unsigned int sizeTree = pow(b, k) - 1;	
	int* tree_host = new int[sizeTree];
	tree_host[0] = ex_p_host->getInicial();

	//Estableciendo la tarjeta
	cudaSetDevice(0);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	printf("El nombre es: %s \n", prop.name);

	//Voy a asignar memoria para el arbol de dependencias.
	int* tree_dev;
	cudaMalloc((void**) &tree_dev, sizeTree*sizeof(int));
	//cudaMemcpy(tree_dev, tree_host, sizeof(int), cudaMemcpyHostToDevice);	

	//Voy asignar memoria para el los parametros extras
	WeightBenefits* ex_p_dev;
	cudaMalloc((void**) &ex_p_dev, sizeof(WeightBenefits));
	cudaMemcpy(ex_p_dev, ex_p_host, sizeof(WeightBenefits), cudaMemcpyHostToDevice);

	for(unsigned int i=0; i < k-1;i++){
		unsigned int nactive = pow(b, i);
		int nb = (int)ceil(nactive/((float)1024));
		int nt = (int)ceil(nactive/((float)nb));
		stage_top_down<<<nb, nt>>>(tree_dev, ex_p_dev, i);
	}

	for(int i=k-1; i>=0; i++){
		unsigned int nactive = pow(b, i);
		int nb = (int)ceil(nactive/((float)1024));
		int nt = (int)ceil(nactive/((float)nb));
		stage_top_down<<<nb, nt>>>(tree_dev, ex_p_dev, i);
	}

	stage_top_down<<<1,1>>>(tree_dev,0, 0);
	cudaDeviceSynchronize();
	cudaCheckForError();

	//Asignar memoria y recuperar valores del arbol
	cudaMemcpy(tree_host, tree_dev, sizeTree*sizeof(int), cudaMemcpyDeviceToHost);

	for(unsigned int i=0; i <sizeTree; i++){
		std::cout<<tree_host[i]<<" ";
	}
	std::cout<<std::endl;

	//Elimino memoria
	cudaFree(tree_dev);
	cudaFree(ex_p_dev);
	free(tree_host);
}

__device__ void Knapsack(unsigned int i, unsigned int j, Matrix<unsigned int>* M, WeightBenefits* ex_p){
	if(i == 0 || j == 0){
		M->setAt(i, j, 0);
	}else{
		if(j < ex_p->w[i - 1]){
			M->setAt(i, j, M->getAt(i-1, j) );
		}else{
			M->setAt(i, j, max(M->getAt(i-1, j), ex_p->b[i-1]+M->getAt(i-1,j-ex_p->w[i-1])));
		}
	}
}

__global__ void dp_by_rows_stage(Matrix<unsigned int>* M_device, WeightBenefits* ex_p_device, unsigned int row){
	unsigned int id = threadIdx.x + blockIdx.x*blockDim.x;

	if( id < M_device->m ){
		Knapsack(row, id, M_device, ex_p_device);
	}
}

void dp_by_rows(Matrix<unsigned int>* M_host,  WeightBenefits* ex_p_host){
	cudaSetDevice(0);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	printf("El nombre es: %s \n", prop.name);


	//Guardo la direccion del arreglo del Host
	unsigned int* arr_M_host = M_host->M;
	
	//Asigno memoria para la Matrix en el Device
	Matrix<unsigned int>* M_device;
	cudaMalloc((void**) &M_device, sizeof(Matrix<unsigned int>));

	//Asigno memoria para el arreglo en el Device.
	unsigned int* arr_M_device;
	cudaMalloc((void**) &arr_M_device, (M_host->n*M_host->m)*sizeof(unsigned int));


	//Intercambio la direccion del arreglo del host por la direccion en el Device.
	M_host->M = arr_M_device;
	//Copio la Matrix del Host al Device. NOTA: NO EL CONTENIDO DE LA MATRIX SOLO LOS APUNTADORES
	cudaMemcpy(M_device, M_host, sizeof(Matrix<unsigned int>), cudaMemcpyHostToDevice);

	//Asignando memoria y copiando los parametros extras. 
	WeightBenefits* ex_p_device;
	cudaMalloc((void**)&ex_p_device, sizeof(WeightBenefits));

	//Copio los parametros del Host al Device
	cudaMemcpy((void*)ex_p_device, (void*) ex_p_host, sizeof(WeightBenefits), cudaMemcpyHostToDevice);

	
	//MEDIR TIEMPO
	double suma =0;
	double N = 100;
	for(unsigned int i=0; i <= N; i++){
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		for(unsigned int row = 0; row < M_host->n; row++){
			dp_by_rows_stage<<<3072, 1>>>(M_device, ex_p_device, row);		
		}
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);

		cudaCheckForError();
		if(i!=0){
			float time;
			cudaEventElapsedTime(&time, start, stop);
			//printf("Tiempo de ejecucion: %f ms\n", time);
			suma+=time;
		}
	}
	printf("Tiempo de ejecucion promedio: %f ms\n", suma/N);



	//Copiando el arreglo de la Matriz que se lleno en Device a host
	cudaMemcpy((void*)arr_M_host, (void*) arr_M_device, (M_host->n*M_host->m)*sizeof(unsigned int),cudaMemcpyDeviceToHost);

	M_host->M = arr_M_host;

	//Liberando el arreglo del Device
	cudaFree(arr_M_device);


	//Liberando la Matriz del Device
	cudaFree(M_device);


	//Liberando la estructura del Device.
	cudaFree(ex_p_device);	
}

void objectsToConsider(unsigned int i, unsigned int j, Matrix<unsigned int>* M, WeightBenefits* ex_p){
	if(i > 0){
		if(j < ex_p->w[i - 1]){
			objectsToConsider(i - 1, j, M, ex_p);
		}else{
			if(M->getAt(i-1, j - ex_p->w[i - 1]) + ex_p->b[i - 1] > M->getAt(i - 1, j)){
				objectsToConsider(i - 1, j - ex_p->w[i - 1],  M, ex_p);
				cout << "P" << i;
			}else{
				objectsToConsider(i - 1, j,  M, ex_p);
			}
		}
	}
}*/

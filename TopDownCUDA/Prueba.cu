#include <iostream>
#include <stdio.h>
using namespace std;


void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){
		cout << "CUDA error: " << cudaGetErrorString(error) << endl;
		exit(EXIT_FAILURE);
	}
}


__global__ void doWork(double* arr_dev){
	int id = threadIdx.x + blockIdx.x*blockDim.x;
	if(id<3072){
		arr_dev[id]=0;

		int pos=0;
		for(unsigned int i=0; i<=999999; i++){
			arr_dev[id]+=1+arr_dev[(id+pos++)%3072];
		}
	}
	
	//arr_dev[id]=suma;
	/*if(id==0){
		printf("El valor es: %f\n", arr_dev[0]);
	}*/
}

int main(){
	double N = 100;
	double suma=0;

	cudaSetDevice(0);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	printf("El nombre es: %s \n", prop.name);

	double* arr_dev;
	cudaMalloc((void**)&arr_dev, 3072*sizeof(double));
	
	for(int i=0; i<=N; i++){
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);	
		
		cudaEventRecord(start, 0);
		doWork<<<31, 100>>>(arr_dev);

		cudaEventRecord(stop, 0);	
		cudaEventSynchronize(stop);
		cudaCheckForError();
				
		if(i!=0){
			float time;
			cudaEventElapsedTime(&time, start, stop);
			suma+=time;	
		}
		cudaEventDestroy(start);
		cudaEventDestroy(stop);
	}
	cudaFree(arr_dev);
	printf("Tiempo promedio: %f ms\n", suma/N);

	return 0;
}

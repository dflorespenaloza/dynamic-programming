#include <iostream>
#include <stdio.h>


void cudaCheckForError(){
	cudaError_t error = cudaGetLastError();

 	if(error != cudaSuccess){
		std::cout << "CUDA error: " << cudaGetErrorString(error) << std::endl;
		exit(EXIT_FAILURE);
	}
}

__global__ void stage_top_down(unsigned int* tree, int parentLabel){
	unsigned int id = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int label = parentLabel + id + 1;

	if(label < 7){
		stage_top_down<<<1, 2>>>(tree, 2*label);
		cudaDeviceSynchronize();

		tree[label] = tree[2*label+1] +tree[2*label+2];
	}else{
		tree[label] = label;
	}
	printf("Ya escribio thread: %d \n",label);
}

void top_down(){
	cudaSetDevice(0);
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	printf("El nombre es: %s \n", prop.name);


	unsigned int k = 4;//Niveles del arbol
	unsigned int b = 2; //Hijos por Nodo
	unsigned int sizeTree = pow(b, k) - 1;	

	//Voy a asignar memoria para el arbol de dependencias.
	unsigned int* tree_dev;
	cudaMalloc((void**) &tree_dev, sizeTree*sizeof(unsigned int));
	cudaMemset(tree_dev, sizeTree*sizeof(unsigned int), 0);

	//Mandar a llamar el Kernel
	stage_top_down<<<1,1>>>(tree_dev, -1);

	cudaDeviceSynchronize();
	cudaCheckForError();



	unsigned int* tree_host = new unsigned int[sizeTree];
	//Asignar memoria y recuperar valores del arbol
	cudaMemcpy(tree_host, tree_dev, sizeTree*sizeof(unsigned int), cudaMemcpyDeviceToHost);

	unsigned int z=1;
	unsigned int corte =pow(2,1) - 1;

	for(unsigned int i=0; i <sizeTree; i++){
		std::cout<<"["<<i<<", " << tree_host[i]<<"] ";
		if(i>=corte){
			corte=pow(2,++z)-1;
			std::cout<<std::endl;
		}
	}
	std::cout<<std::endl;

	//Elimino memoria
	cudaFree(tree_dev);
	//cudaFree(ex_p_dev);
	free(tree_host);
}

int main(){
	printf("Version 3 \n");
	top_down();
	return EXIT_SUCCESS;
}
